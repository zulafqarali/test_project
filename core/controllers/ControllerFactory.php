<?php

namespace Core\Controllers;

/**
 * Created by PhpStorm.
 * User: zulaf
 * Date: 10/02/2017
 * Time: 00:22
 */
class ControllerFactory
{
    public static function redirect($url,$message,$wait = 0){
        $controller = new Controller();
        $controller->redirect($url, $message, $wait);
    }
}