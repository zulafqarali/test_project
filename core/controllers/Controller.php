<?php

namespace Core\Controllers;

use Core\Libraries\Database\Database;
use Core\Libraries\Loader;
use Core\Views\ViewManager;

/**
 * Created by PhpStorm.
 * User: zulaf
 * Date: 09/02/2017
 * Time: 23:36
 */
class Controller
{
// Base Controller has a property called $loader, it is an instance of Loader class(introduced later)

    protected $loader;

    protected  $view;

    protected $db;


    public function __construct(){

        $this->loader = new Loader();

        $this->view = ViewManager::getInstance();

        $this->db = Database::getInstance();

    }


    public function redirect($url,$message,$wait = 0){

        if ($wait == 0){

            header("Location:$url");

        } else {

            include CURR_VIEW_PATH . "message.html";

        }


        exit;

    }
}