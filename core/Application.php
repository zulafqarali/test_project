<?php

namespace Core;


/**
 * Created by PhpStorm.
 * User: zulaf
 * Date: 09/02/2017
 * Time: 23:14
 */
class Application
{
    protected $router;
    public function __construct(){
        $this->router = Router::getInstance();
    }

    public function run(){

        $this->init();

        $this->autoload();

        $this->dispatch();
    }


    private function init() {
        define("DS", DIRECTORY_SEPARATOR);

        define("ROOT", getcwd() . DS);

        define("APP_PATH", ROOT . 'app' . DS);


        define("PUBLIC_PATH", "public" . DS);


        define("CONFIG_PATH", APP_PATH . DS);


        define("VIEW_PATH", APP_PATH . "views" . DS);

        include CONFIG_PATH . "config.php";
        include CONFIG_PATH . "Routes.php";

        //$this->router->add('/', ['controller' => 'HomeController', 'action' => 'index']);

        session_start();

    }


    private function autoload() {
        spl_autoload_register(array(__CLASS__,'load'));
    }

    private function load($classname){

        $className = ltrim($classname, '\\');
        $fileName  = '';
        $namespace = '';
        if ($lastNsPos = strrpos($className, '\\')) {
            $namespace = substr($className, 0, $lastNsPos);
            $className = substr($className, $lastNsPos + 1);
            $fileName  = str_replace('\\', DS, $namespace) . DS;
        }
        $fileName .= str_replace('_', DS, $className) . '.php';

        require $fileName;



    }


    private function dispatch() {
        /*echo "<pre>";
        htmlspecialchars(print_r($this->router->getRoutes()), true);
        echo "</pre>";
        if($this->router->match($_SERVER['QUERY_STRING'])) {
            echo "<pre>";
            htmlspecialchars(print_r($this->router->getParams()), true);
            echo "</pre>";
        }else{
            echo "url not found";
        }*/

        //exit;

        $url = !empty($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : '/';



        $this->router->dispatch($url);

    }

    private static $application_singleton = NULL;
    public static function getInstance() {
        if (self::$application_singleton  == null) {
            self::$application_singleton  = new Application();
        }
        return self::$application_singleton ;
    }
}