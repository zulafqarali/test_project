<?php

namespace Core\Helpers;

/**
 * Created by PhpStorm.
 * User: zulaf
 * Date: 11/02/2017
 * Time: 17:50
 */

class Helper
{

    public static function getConfig($key = null)
    {
        if($key === null){
            return $GLOBALS['config'];
        }
        
        if (in_array($key, $GLOBALS['config'])) {
            return $GLOBALS['config'][$key];
        }

        return false;
    }
    
    public static function public_path(){
        return PUBLIC_PATH;
    }

    public static function base_url(){
        return $GLOBALS['config']['base_url'];
    }

}