<?php
namespace Core\Libraries;
/**
 * Created by PhpStorm.
 * User: zulaf
 * Date: 09/02/2017
 * Time: 23:46
 */
class Loader
{
// Load library classes

    public function library($lib){

        include LIB_PATH . "$lib.php";

    }


    // loader helper functions. Naming conversion is xxx.php;

    public function helper($helper){

        include HELPER_PATH . "{$helper}.php";

    }
}