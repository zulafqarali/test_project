<?php

namespace Core\Models;

/**
 * Created by PhpStorm.
 * User: zulaf
 * Date: 10/02/2017
 * Time: 00:32
 */
class ModelFactory
{
    public static function make($model){
        $model = "App\Models\\".$model;
        if(!class_exists($model)){
            throw new Exception("Model not found");
        }
        return new $model;
    }
}