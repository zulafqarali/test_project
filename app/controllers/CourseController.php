<?php
/**
 * Created by PhpStorm.
 * User: TxLabz
 * Date: 2/16/2017
 * Time: 3:32 PM
 */

namespace App\Controllers;


use Core\Controllers\Controller;
use Core\Helpers\Helper;
use Core\Models\ModelFactory;

class CourseController extends Controller
{
    public function index(){
        $courses = ModelFactory::make("Course")->all();
        $this->view->setVariable("courses", $courses);
        $this->view->render("course/index");
    }

    public function add(){
        $this->view->render("course/add");
    }

    public function update(){
        $id = $_GET['id'];
        $course = ModelFactory::make("Course")->find($id);
        $this->view->setVariable("course", $course);
        $this->view->render("course/add");
    }

    public function delete(){
        $id = $_GET['id'];
        ModelFactory::make("Course")->delete($id);
        $this->view->setFlash("Course is Deleted");
        $this->redirect(Helper::base_url()."course",'');
    }

    public function store(){
        $input = $_POST;
        $course = ModelFactory::make("Course");
        if(isset($input['id'])){
            $id = $course->update([
                'id' => $input['id'],
                'title' => $input['title'],
                'code' => $input['code'],
                'credit_hours' => $input['credit_hours']
            ]);
            $this->view->setFlash("Course is Updated");
            $this->redirect(Helper::base_url()."course",'');
        }
        $id = $course->save([
            'title' => $input['title'],
            'code' => $input['code'],
            'credit_hours' => $input['credit_hours']
        ]);

        if($id > 0){
            $this->view->setFlash("Course is added");
            $this->redirect(Helper::base_url()."course",'');
        }
    }
}