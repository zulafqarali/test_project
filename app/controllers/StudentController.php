<?php
/**
 * Created by PhpStorm.
 * User: TxLabz
 * Date: 2/15/2017
 * Time: 4:19 PM
 */

namespace App\Controllers;


use Core\Controllers\Controller;
use Core\Helpers\Helper;
use Core\Models\ModelFactory;

class StudentController extends Controller
{
    public function index(){
        $students = ModelFactory::make("Student")->all();
        $this->view->setVariable("students", $students);
        $this->view->render("student/index");
    }

    public function add(){
        $this->view->render("student/add");
    }

    public function update(){
        $id = $_GET['id'];
        $student = ModelFactory::make("Student")->find($id);
        $this->view->setVariable("student", $student);
        $this->view->render("student/add");
    }

    public function delete(){
        $id = $_GET['id'];
        ModelFactory::make("Student")->delete($id);
        $this->view->setFlash("Student is Deleted");
        $this->redirect(Helper::base_url()."student",'');
    }

    public function store(){
        $input = $_POST;
        $student = ModelFactory::make("Student");
        if(isset($input['id'])){
            $id = $student->update([
                'id' => $input['id'],
                'name' => $input['name'],
                'email' => $input['email'],
                'contact' => $input['contact']
            ]);
            $this->view->setFlash("Student is Updated");
            $this->redirect(Helper::base_url()."student",'');
        }
        $id = $student->save([
            'name' => $input['name'],
            'email' => $input['email'],
            'contact' => $input['contact']
        ]);

        if($id > 0){
            $this->view->setFlash("Student is added");
            $this->redirect(Helper::base_url()."student",'');
        }
    }
}