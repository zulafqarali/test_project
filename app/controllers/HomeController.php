<?php

namespace App\Controllers;

use Core\Controllers\Controller;
use Core\Helpers\Helper;
use Core\Models\ModelFactory;

/**
 * Created by PhpStorm.
 * User: zulaf
 * Date: 10/02/2017
 * Time: 00:39
 */
class HomeController extends Controller
{
    public function index(){
        $students = ModelFactory::make("Student")->all();
        $teachers = ModelFactory::make("Teacher")->all();
        $courses = ModelFactory::make("Course")->all();
        $this->view->setVariable("students", $students);
        $this->view->setVariable("teachers", $teachers);
        $this->view->setVariable("courses", $courses);
        $this->view->render("index");
    }

}