<?php
/**
 * Created by PhpStorm.
 * User: TxLabz
 * Date: 2/16/2017
 * Time: 3:25 PM
 */

namespace App\Controllers;


use Core\Controllers\Controller;
use Core\Helpers\Helper;
use Core\Models\ModelFactory;

class TeacherController extends Controller
{
    public function index(){
        $teachers = ModelFactory::make("Teacher")->all();
        $this->view->setVariable("teachers", $teachers);
        $this->view->render("teacher/index");
    }

    public function add(){
        $this->view->render("teacher/add");
    }

    public function update(){
        $id = $_GET['id'];
        $teacher = ModelFactory::make("Teacher")->find($id);
        $this->view->setVariable("teacher", $teacher);
        $this->view->render("teacher/add");
    }

    public function delete(){
        $id = $_GET['id'];
        ModelFactory::make("Teacher")->delete($id);
        $this->view->setFlash("Teacher is Deleted");
        $this->redirect(Helper::base_url()."teacher",'');
    }

    public function store(){
        $input = $_POST;
        $teacher = ModelFactory::make("Teacher");
        if(isset($input['id'])){
            $id = $teacher->update([
                'id' => $input['id'],
                'name' => $input['name'],
                'email' => $input['email'],
                'contact' => $input['contact']
            ]);
            $this->view->setFlash("Teacher is Updated");
            $this->redirect(Helper::base_url()."teacher",'');
        }
        $id = $teacher->save([
            'name' => $input['name'],
            'email' => $input['email'],
            'contact' => $input['contact']
        ]);

        if($id > 0){
            $this->view->setFlash("Teacher is added");
            $this->redirect(Helper::base_url()."teacher",'');
        }
    }
}