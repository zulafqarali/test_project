<?php


use Core\Router;

$router = Router::getInstance();

$router->add('/', ['controller' => 'HomeController', 'action' => 'index']);
$router->add('student', ['controller' => 'StudentController', 'action' => 'index']);
$router->add('student/add', ['controller' => 'StudentController', 'action' => 'add']);
$router->add('student/store', ['controller' => 'StudentController', 'action' => 'store']);
$router->add('student/update', ['controller' => 'StudentController', 'action' => 'update']);
$router->add('student/delete', ['controller' => 'StudentController', 'action' => 'delete']);

$router->add('teacher', ['controller' => 'TeacherController', 'action' => 'index']);
$router->add('teacher/add', ['controller' => 'TeacherController', 'action' => 'add']);
$router->add('teacher/store', ['controller' => 'TeacherController', 'action' => 'store']);
$router->add('teacher/update', ['controller' => 'TeacherController', 'action' => 'update']);
$router->add('teacher/delete', ['controller' => 'TeacherController', 'action' => 'delete']);


$router->add('course', ['controller' => 'CourseController', 'action' => 'index']);
$router->add('course/add', ['controller' => 'CourseController', 'action' => 'add']);
$router->add('course/store', ['controller' => 'CourseController', 'action' => 'store']);
$router->add('course/update', ['controller' => 'CourseController', 'action' => 'update']);
$router->add('course/delete', ['controller' => 'CourseController', 'action' => 'delete']);
