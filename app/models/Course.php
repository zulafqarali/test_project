<?php
/**
 * Created by PhpStorm.
 * User: TxLabz
 * Date: 2/15/2017
 * Time: 11:24 AM
 */

namespace App\Models;


use Core\Models\Model;

class Course extends Model
{
    protected $table = "courses";

    public function __construct(){
        parent::__construct($this->table);
    }
}