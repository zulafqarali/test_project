<?php
/**
 * Created by PhpStorm.
 * User: TxLabz
 * Date: 2/15/2017
 * Time: 11:19 AM
 */

namespace App\Models;


use Core\Models\Model;

class Teacher extends Model
{
    protected $table = "teachers";

    public function __construct(){
        parent::__construct($this->table);
    }
}