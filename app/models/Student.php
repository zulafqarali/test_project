<?php

namespace App\Models;

use Core\Models\Model;

/**
 * Created by PhpStorm.
 * User: zulaf
 * Date: 09/02/2017
 * Time: 23:53
 */
class Student extends Model
{
    protected $table = "students";

    public function __construct(){
        parent::__construct($this->table);
    }
}