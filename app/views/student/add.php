<?php
$view = \Core\Views\ViewManager::getInstance();
$student = $view->getVariable("student");

?>

<!-- Page Content -->
<div class="container">

    <div class="row">

        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>Add Student</strong>
                    <span class="pull-right clickable"><a href="student" class="">Cancel</a> </span>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="student/store">
                        <?php if(isset($student)){ ?>
                            <input type="hidden" name="id" value="<?php echo $student['id']; ?>">
                        <?php } ?>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="name">Name *:</label>
                            <div class="col-sm-10">
                                <input value="<?php echo isset($student)? $student['name'] : '' ?>" type="text" name="name" class="form-control" id="name" placeholder="Enter name" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2" for="email">Email:</label>
                            <div class="col-sm-10">
                                <input value="<?php echo isset($student)? $student['email'] : '' ?>" type="email" name="email" class="form-control" id="email" placeholder="Enter email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="contact">Contact:</label>
                            <div class="col-sm-10">
                                <input value="<?php echo isset($student)? $student['contact'] : '' ?>" type="text" name="contact" class="form-control" id="contact" placeholder="Enter contact">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


        </div>

    </div>

</div>
<!-- /.container -->