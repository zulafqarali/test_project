<?php
//file: view/layouts/default.php
use Core\Views\ViewManager;

$view = ViewManager::getInstance();

$teachers = $view->getVariable("teachers");

//print_r($students);
?>

<!-- Page Content -->
<div class="container">

    <div class="row">

        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>Teachers</strong>
                    <span class="pull-right clickable"><a href="teacher/add" class="">Add</a> </span>
                </div>
                <div class="panel-body">
                    <?php if(count($teachers) > 0) { ?>
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Contact</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($teachers as $teacher){ ?>
                                <tr>
                                    <td><?php echo $teacher['id']; ?></td>
                                    <td><?php echo $teacher['name']; ?></td>
                                    <td><?php echo $teacher['email']; ?></td>
                                    <td><?php echo $teacher['contact']; ?></td>
                                    <td><a href="teacher/update?id=<?php echo $teacher['id']; ?>">Update</a> | <a href="teacher/delete?id=<?php echo $student['id']; ?>">Delete</a> </td>
                                </tr>
                            <?php }  ?>
                            </tbody>
                        </table>
                    <?php } else echo "No Record Found"; ?>
                </div>
            </div>

        </div>

    </div>

</div>
<!-- /.container -->