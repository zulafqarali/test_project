<?php
//file: view/layouts/default.php
use Core\Views\ViewManager;

$view = ViewManager::getInstance();

$courses = $view->getVariable("courses");

//print_r($courses);
?>

<!-- Page Content -->
<div class="container">

    <div class="row">

        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>Courses</strong>
                    <span class="pull-right clickable"><a href="course/add" class="">Add</a> </span>
                </div>
                <div class="panel-body">
                    <?php if(count($courses) > 0) { ?>
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Code</th>
                                <th>Credit Hours</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($courses as $course){ ?>
                                <tr>
                                    <td><?php echo $course['id']; ?></td>
                                    <td><?php echo $course['title']; ?></td>
                                    <td><?php echo $course['code']; ?></td>
                                    <td><?php echo $course['credit_hours']; ?></td>
                                    <td><a href="course/update?id=<?php echo $course['id']; ?>">Update</a> | <a href="course/delete?id=<?php echo $course['id']; ?>">Delete</a> </td>
                                </tr>
                            <?php }  ?>
                            </tbody>
                        </table>
                    <?php } else echo "No Record Found"; ?>
                </div>
            </div>

        </div>

    </div>

</div>
<!-- /.container -->