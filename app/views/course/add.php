<?php
$view = \Core\Views\ViewManager::getInstance();
$course = $view->getVariable("course");

?>

<!-- Page Content -->
<div class="container">

    <div class="row">

        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>Add Student</strong>
                    <span class="pull-right clickable"><a href="course" class="">Cancel</a> </span>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="course/store">
                        <?php if(isset($course)){ ?>
                            <input type="hidden" name="id" value="<?php echo $course['id']; ?>">
                        <?php } ?>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="title">Title *:</label>
                            <div class="col-sm-10">
                                <input value="<?php echo isset($course)? $course['title'] : '' ?>" type="text" name="title" class="form-control" id="title" placeholder="Enter title" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2" for="code">Code:</label>
                            <div class="col-sm-10">
                                <input value="<?php echo isset($course)? $course['code'] : '' ?>" type="code" name="code" class="form-control" id="code" placeholder="Enter code">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="credit_hours">Credit Hours:</label>
                            <div class="col-sm-10">
                                <input value="<?php echo isset($course)? $course['credit_hours'] : '' ?>" type="text" name="credit_hours" class="form-control" id="credit_hours" placeholder="Enter credit_hours">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


        </div>

    </div>

</div>
<!-- /.container -->