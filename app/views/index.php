<?php
//file: view/layouts/default.php
use Core\Views\ViewManager;

$view = ViewManager::getInstance();

$students = $view->getVariable("students");
$teachers = $view->getVariable("teachers");
$courses = $view->getVariable("courses");

//print_r($students);
?>

<!-- Page Content -->
<div class="container">

    <div class="row">

        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>Students</strong>
                    <span class="pull-right clickable"><a href="student/add" class="">Add</a> </span>
                </div>
                <div class="panel-body">
                    <?php if(count($students) > 0) { ?>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Contact</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($students as $student){ ?>
                        <tr>
                            <td><?php echo $student['id']; ?></td>
                            <td><?php echo $student['name']; ?></td>
                            <td><?php echo $student['email']; ?></td>
                            <td><?php echo $student['contact']; ?></td>
                            <td><a href="student/update?id=<?php echo $student['id']; ?>">Update</a> | <a href="student/delete?id=<?php echo $student['id']; ?>">Delete</a> </td>
                        </tr>
                        <?php }  ?>
                        </tbody>
                    </table>
                    <?php } else echo "No Record Found"; ?>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>Teachers</strong>
                    <span class="pull-right clickable"><a href="teacher/add" class="">Add</a> </span>
                </div>
                <div class="panel-body">
                    <?php if(count($teachers) > 0) { ?>
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Contact</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($teachers as $teacher){ ?>
                                <tr>
                                    <td><?php echo $teacher['id']; ?></td>
                                    <td><?php echo $teacher['name']; ?></td>
                                    <td><?php echo $teacher['email']; ?></td>
                                    <td><?php echo $teacher['contact']; ?></td>
                                    <td><a href="student/update?id=<?php echo $teacher['id']; ?>">Update</a> | <a href="student/delete?id=<?php echo $teacher['id']; ?>">Delete</a> </td>
                                </tr>
                            <?php }  ?>
                            </tbody>
                        </table>
                    <?php } else echo "No Record Found"; ?>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>Courses</strong>
                    <span class="pull-right clickable"><a href="course/add" class="">Add</a> </span>
                </div>
                <div class="panel-body">
                    <?php if(count($courses) > 0) { ?>
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>code</th>
                                <th>Credit Hours</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($courses as $course){ ?>
                                <tr>
                                    <td><?php echo $course['id']; ?></td>
                                    <td><?php echo $course['title']; ?></td>
                                    <td><?php echo $course['code']; ?></td>
                                    <td><?php echo $course['credit_hours']; ?></td>
                                    <td><a href="student/update?id=<?php echo $course['id']; ?>">Update</a> | <a href="student/delete?id=<?php echo $course['id']; ?>">Delete</a> </td>
                                </tr>
                            <?php }  ?>
                            </tbody>
                        </table>
                    <?php } else echo "No Record Found"; ?>
                </div>
            </div>

        </div>

    </div>

</div>
<!-- /.container -->