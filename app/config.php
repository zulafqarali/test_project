<?php
/**
 * Created by PhpStorm.
 * User: zulaf
 * Date: 09/02/2017
 * Time: 23:03
 */

$GLOBALS['config']['db'] = [
    'host' =>  'localhost',
    'user' =>   'root',
    'password' =>   '',
    'dbname' => 'testdb',
    'port'  =>  '3306',
    'charset' => 'utf8'
];

$GLOBALS['config']['base_url'] = "http://localhost/testproject/";
