<?php
/**
 * Created by PhpStorm.
 * User: zulaf
 * Date: 09/02/2017
 * Time: 23:10
 */

require 'vendor/autoload.php';

use Core\Application;

$app = Application::getInstance();

$app->run();